const state = {
  drawer: false,
  drawerMini: false,
};

const getters = {
  getDrawer: (state) => state.drawer,
  getDrawerMini: (state) => state.drawerMini,
};

const mutations = {
  SET_DRAWER(state, value) {
    state.drawer = value;
  },
  SET_DRAWER_MINI(state, value) {
    state.drawerMini = value;
  },
};

const actions = {
  toggleDrawer(commit, value) {
    commit('SET_DRAWER', value);
  },
  toggleDrawerMini(commit, value) {
    commit('SET_DRAWER_MINI', value);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
